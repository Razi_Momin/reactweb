import React, { Component } from 'react'
import { connect } from 'react-redux'

export class reduxCheck extends Component {
    render() {
        return (
            <div>

            </div>
        )
    }
}

const mapStateToProps = (state) => ({

    myname: state.name,
    age: state.age,
    wishes: state.wishes,

})

const mapDispatchToProps = (dispatch) => {
  return {
    changeName: (name) => {
      dispatch({
        type: 'CHANGE_NAME',
        payload: name
      })
    },
    changeAge: (age) => {
      dispatch({
        type: 'CHANGE_AGE',
        payload: age
      })
    }
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(reduxCheck)
