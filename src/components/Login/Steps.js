
import React, { useState } from 'react';
export default function Steps({ step }, props) {
    console.log("STEP value" + step);
    const [step2, serStep2] = useState(false);
    // let complete = 'complete';
    let disabledclass = (step == 1) ? 'disabled' : '';
    let complete = (step != 1) ? 'complete' : '';
    return (
        <div className="row bs-wizard" style={{ borderBottom: 0 }}>
            <div className="col-6 bs-wizard-step complete">
                <div className="text-center bs-wizard-stepnum"></div>
                <div className="progress">
                    <div className="progress-bar"></div>
                </div>
                <a href="#" className="bs-wizard-dot">
                    <span>
                        <div>Step</div>
                                    1
                                </span>
                </a>
            </div>
            <div className={`col-6 bs-wizard-step ${disabledclass} ${complete}`}>
                <div className="text-center bs-wizard-stepnum"></div>
                <div className="progress shop-progress">
                    <div className="progress-bar"></div>
                </div>
                <a href="#" className="bs-wizard-dot">
                    <span>
                        <div>Step</div>
                                    2
                                </span>
                </a>
            </div>
        </div>
    )
}
