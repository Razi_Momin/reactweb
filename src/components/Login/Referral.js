import React, { Component } from 'react'

export default class Referral extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stateData: [],
            referralShow: props.referralShow
        }
    }
    handleClose = (e) => {
        this.setState({
            referralShow: false
            // stateDataabc: { comments: response.data.states }
        })
    }
    render() {
        console.log(this.props);
        return (
            
            this.state.referralShow ?
                <div onClick={this.handleClose} className="referral_flip">
                    <div className="flip_child">
                        <form className="referral_form">
                            <div className="text-center bold form-group">Enter Referral Code</div>
                            <div className="custom_focus">
                                <input type="text" className="form-group ajax-input step2_referral" />
                            </div>
                            <div className="f-10 form-group">
                                <span className="bold" style={{ color: 'red' }}>Note</span>
                    The user who has referred you will receive 10 % of your entry fee.The user who has referred your referrer will receive 5 % of your entry fee.
                </div>
                            <button type="submit" className="ppm_btn_blue center-block referral_code_btn">Apply</button>
                        </form>
                    </div>
                </div>
                : null
        )
    }
}
