import React, { Component } from 'react';
import SignUp from 'components/Login/SignUp';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
export default class LoginMain extends Component {
    render() {
        return (
            <div>
                <div className="bg_img"></div>
                <div className="mobile_bg_opacity"></div>
                <div className="regmiddle">
                    <div className="container log_cont">

                        <div className="clearfix"></div>
                        <div className="login_center relative">
                            <div className="text-center">
                                <div className="bold">SIGN-IN WITH</div>
                                <div style={{ margin: "10px 0" }} className="bold">
                                    <div className="verticle_middle social_btn_c bg-white">
                                        <span className="soc_img verticle_middle">
                                            <img src={require('assets/icons/facebook.png')} />
                                        </span>
                                        <span className="verticle_middle soc_text text-center">
                                            Facebook
                    </span>
                                    </div>

                                    <div className="verticle_middle social_btn_c bg-white">
                                        <span className="soc_img verticle_middle">
                                            <img src={require('assets/icons/google_new.png')} />
                                        </span>
                                        <span className="verticle_middle soc_text text-center">
                                            Google
                                    </span>
                                    </div>
                                </div>
                                <div className="clearfix"></div>
                                <div className="bold">OR</div>
                            </div>
                            <div className="login_box shadow relative bg-white">
                                <div className="">
                                    <div className="signin_left">Sign in</div>
                                    <div className="pull-right f-10 signin_right bold">Get upto 10% Bonus on sign-up</div>
                                </div>
                                <div className="clearfix"></div>
                                <form className="sign_in_form">
                                    <div className="custom_focus">
                                        <label className="ajaxlabel">Enter your registered mobile number</label>
                                        <input type="tel" className="ajax-input login_mobile integer" maxLength="10" />
                                    </div>
                                    <div className="custom_focus">
                                        <div className="password_h_show" data-id="0">
                                            <img className="height-20 vertically-center-bottom" src={require('assets/icons/hide_password.png')} />
                                        </div>
                                        <label className="ajaxlabel">Password</label>
                                        <input type="password" className="ajax-input password login_password" />
                                    </div>
                                    <div className="clearfix"></div>
                                    <br /><br />
                                    <div className="text-center form-group">
                                        <span className="f-12 bold forg_pass" >FORGOT PASSWORD ?</span>
                                    </div>
                                    <br />
                                    <div className="text-center">
                                        <input type="submit" value="LOGIN" className="ppm_btn_blue log_bt" />
                                    </div>
                                </form>
                                <br />
                                <span className="trouble_login text-center f-10 btn-block bold">TROUBLE WITH LOGIN ?</span>
                            </div>
                            {

                                <Switch>
                                    <Route path="/SignUp">
                                        <SignUp />
                                    </Route>
                                </Switch>


                            }
                            {/* <div>       <Link to="/SignUp">Users</Link></div> */}
                            <div className="text-center reg_now bold">
                                Not a member yet ? <span className="register_now bold">
                                    <Link to="/SignUp">Sign-Up</Link>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
