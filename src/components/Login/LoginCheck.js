import React, { Component } from 'react'
import LoginMain from 'components/Login/LoginMain';
import SignUp from 'components/Login/SignUp';
import SocialLogin from 'components/Login/SocialLogin';
import LoginBg from 'components/Login/LoginBg';
import { webService } from 'components/common/webService';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import { Redirect } from "react-router-dom";
import { browserHistory } from 'react-router';
export default class LoginCheck extends Component {
    constructor(props) {
        super();
        this.state = {
            UserExist: false,
            CheckStatus: false,
            mobile_no: ''
        }
        // console.log(gVars.API_BASE_URL);
    }
    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleSubmit = (e) => {
        e.preventDefault();
        let MobileNo = this.state.mobile_no.trim();
        // alert(MobileNo.length);
        if (MobileNo == '' || MobileNo.length !== 10) {
            alert('Please enter valid mobile number');
        } else {
            let apiEndpoint = 'Oauth/primaryStep';
            let header = webService.getHeaders({})
            let postArray = { mobile_no: this.state.mobile_no };
            //console.log(postArray)
            webService.post(apiEndpoint, postArray, header).then((response) => {
              
                if (response.status === 200) {
                    response = response.data;
                    // console.log(response);
                    if (response.success == true) {
                        if (response.status_code == 1) {
                            this.setState({ UserExist: true });
                        }
                        this.setState({ CheckStatus: true });
                    }
                } else {
                    alert('Net connection error');
                }
                //console.log(response.data)
            })

        }
        //console.log(this.state);
    }
    render() {
        // alert('render call');
        // console.log(this.state);
        if (this.state.CheckStatus) {
            // alert(this.state.ispass);
            if (this.state.UserExist) {
                return <Redirect to="/loginmain" />
            } else {
                return <Redirect to={{
                    pathname: '/signup',
                    state: { mobile_no: this.state.mobile_no }
                }} />

            }
        }
        return (
            <div>
                <div className="regmiddle">
                    <SocialLogin />
                    <div className="login_box shadow bg-white">
                        <form onSubmit={this.handleSubmit} className="login_check_form">
                            <div className="custom_focus">
                                <label className="ajaxlabel">Enter mobile number</label>
                                <input type="tel" value={this.props.id} id="mobile_no" onChange={this.handleChange} className="ajax-input login_check_input integer ajax-input" maxLength="10" />
                                <label className="f-9 text-gray" >We will guide you with login/Registration process</label>
                            </div>
                            <div className="clearfix"></div>
                            <div className="text-center">
                                <input type="submit" className="ppm_btn_blue log_bt" value="SUBMIT" />
                            </div>
                        </form>
                    </div>
                    <div className="text-center reg_now bold bg-white f-12">
                        Not a member yet ? <span className="cursor text-blue">
                            <Link to="/SignUp">Sign-Up</Link>
                        </span>
                    </div>
                </div>
            </div>
        )
    }
}
