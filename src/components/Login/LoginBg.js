import React from 'react'

export default function LoginBg() {
    return (
        <div>
            <div className="bg_img"></div>
            <div className="mobile_bg_opacity"></div>
        </div>
    )
}
