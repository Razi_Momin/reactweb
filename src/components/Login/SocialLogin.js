import React from 'react'

export default function SocialLogin() {
    return (
        <div className="socialdiv">
            <div className="text-center bold">Sign-in With</div>
            <div className="soccontainer">
                <div className="flex justify-content-center">
                    <div className="flex align-items-center bg-white social_btn_c">
                        <div className="soc_img">
                            <img src={require('assets/icons/facebook.png')} />
                        </div>
                        <div>
                            Facebook
                                </div>
                    </div>
                    <div className="flex align-items-center bg-white social_btn_c">
                        <div className="soc_img">
                            <img src={require('assets/icons/google_new.png')} />
                        </div>
                        <div>
                            Google
                                </div>
                    </div>
                </div>
            </div>
            <div className="text-center">
                <div className="bold">OR</div>
            </div>
        </div>
    )
}
