import React from 'react'

function StateDropdown({ stateData }) {
    return (
        <option>{stateData.name}</option>
    )
}

export default StateDropdown
