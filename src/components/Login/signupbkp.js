// import React from 'react'
import React, { Component } from 'react';
import SocialLogin from 'components/Login/SocialLogin';
import Steps from 'components/Login/Steps';
import { Button, ButtonToolbar, Modal } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faAngleRight, faCaretRight } from '@fortawesome/free-solid-svg-icons'
export default function SignUp(props) {

    // componentWillReceiveProps(nextProps){
    //     console.log('---------------------', props)
    // }
    // console.log(props);
    // return false;
    const handleChange = (e) => {
        // mobile_no(e.target.value);
        // email(e.target.value);
    }
    const handleClose = (e) => {
        this.setState({
            show: false
        });
        // mobile_no(e.target.value);
        // email(e.target.value);
    }
    // const [show, setShow] = useState(false);
    // const [mobvalue, mobile_no] = useState();
    // const [emailvalue, email] = useState();
    // const handleClose = () => setShow(false);
    // const handleShow = () => setShow(true);

    // const Wrap = return <div>content</div>
    class SignUp extends Component {
        constructor(props) {
            super(props);
            this.state = {
                show: false
            }
            // console.log('-----------------', props)
        }
        render() {
            return (
                <div className="regmiddle">
                    <SocialLogin />
                    <div style={{ paddingTop: "0" }} className="login_box shadow bg-white relative">
                        <div className="flex justify-content-between align-items-center">
                            <div style={{ color: "#012566" }} className="bold">
                                SignUp
                            </div>
                            <div>
                                <div className="f-10 signin_right text-white">
                                    Get Upto <span>&#8377; </span>100 Bonus Cash on sign-up
                                </div>
                            </div>
                        </div>
                        <Steps />

                        <form onSubmit="" className="login_check_form">
                            <div className="custom_focus">
                                <label className="ajaxlabel">Enter mobile number</label>
                                <input type="tel" onChange={handleChange} id="mobile_no" className="ajax-input login_check_input integer ajax-input" maxLength="10" />
                                <label className="f-9 text-gray" >We will send an OTP to your entered mobile number for verification</label>
                            </div>
                            <div className="custom_focus">
                                <label className="ajaxlabel">Enter email id</label>
                                <input type="email" id="email" onChange={handleChange} className="ajax-input login_check_input integer ajax-input" />
                                <label className="f-9 text-gray" >We will send a link to your entered Email ID for verification</label>
                            </div>
                            <div className="clearfix"></div>

                            {/* <div className="text-center">
                                <input type="submit" className="ppm_btn_blue log_bt" value="SUBMIT" />
                            </div> */}
                            <div onClick={handleShow}>
                                <div className="align-items-center flex justify-content-end">
                                    <div style={{ marginRight: '10px' }}>
                                        <span>Next</span>
                                    </div>
                                    <div>
                                        <FontAwesomeIcon style={{ fontSize: "30px" }} icon={faCaretRight} />
                                    </div>
                                </div>
                            </div>
                            <Modal
                                aria-labelledby="contained-modal-title-vcenter"
                                centered show={this.state.show} onHide={handleClose}>
                                <Modal.Header>
                                    <div style={{ color: '#012566' }} className="f-14 bold modal-title text-uppercase">Number Verification</div>
                                </Modal.Header>
                                <Modal.Body className="f-12">The privileges to use the Paytm withdrawal feature will be available only on this number.You will not be able to modify the number again.</Modal.Body>
                                <div style={{ backgroundColor: "#012566" }} className="flex text-white text-center">
                                    <div onClick={handleClose} style={{ borderRight: "1px solid" }} className="col-6 verifsec">Change</div>
                                    <div className="col-6 verifsec">Ok</div>
                                </div>
                            </Modal>
                        </form>
                    </div>
                </div>
            );
        }
    }

}

