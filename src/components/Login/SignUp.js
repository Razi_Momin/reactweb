// import React from 'react'
import React, { Component } from 'react';
import SocialLogin from 'components/Login/SocialLogin';
import Steps from 'components/Login/Steps';
import { Button, ButtonToolbar, Modal } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faAngleRight, faCaretRight } from '@fortawesome/free-solid-svg-icons';
import { webService } from 'components/common/webService';
export default class SignUp extends Component {

    constructor(props) {
        super(props);
        this.state = {
            'show': false,
            'mobile_no': '',
            'email': ''
        };
    }

    handleChange = (e) => {
        this.setState({
            [e.target.id]: e.target.value
        })
    }
    handleClose = (e) => {
        this.setState({
            'show': false
        });
    }
    handleOpen = (e) => {
        this.setState({
            'show': true
        });
    }
    StepValidation1 = (e) => {
        let mobile_no = this.state.mobile_no;
        let email = this.state.email;
        if (mobile_no === "" || mobile_no.length != 10) {
            alert("Mobile Number field cannot be blank");
        } else if (email === "") {
            alert("Please enter valid email id");
        } else {
            this.handleOpen();
        }
    }
    StepConfirm1 = (e) => {
        let mobile_no = this.state.mobile_no;
        let email = this.state.email;
        let apiEndpoint = 'Oauth/signupStepOne';
        let header = webService.getHeaders({})
        let postArray = { mobile_no: mobile_no, email: email, is_email_edit: 1 };
        //console.log(postArray)
        webService.post(apiEndpoint, postArray, header).then((response) => {
            console.log(response);

            if (response.status === 200) {
                response = response.data;
                if (response.success === true) {
                    if (response.status_code === 2) {
                        this.props.history.push({
                            pathname: '/step2',
                            // mobile_no: mobile_no,
                            // search: '?query=abc',
                            state: { mobile_no: mobile_no, email: email }
                        });
                    }
                }
            } else {
                alert('Net connection error');
            }
            //console.log(response.data)
        })
    }

    render() {
        //console.log(this.state);
        // console.log(this.props);
        let mobile_no = (this.props.location.state !== undefined) ? this.props.location.state.mobile_no : '';
        return (
            <div className="regmiddle">
                <SocialLogin />
                <div style={{ paddingTop: "0" }} className="login_box shadow bg-white relative">
                    <div className="flex justify-content-between align-items-center">
                        <div style={{ color: "#012566" }} className="bold">
                            SignUp
                            </div>
                        <div>
                            <div className="f-10 signin_right text-white">
                                Get Upto <span>&#8377; </span>100 Bonus Cash on sign-up
                                </div>
                        </div>
                    </div>
                    <Steps step={1} />

                    <form onSubmit="" className="login_check_form">
                        <div className="custom_focus">
                            <label className="ajaxlabel">Enter mobile number</label>
                            <input type="tel" defaultValue={mobile_no} onChange={this.handleChange} id="mobile_no" className="ajax-input login_check_input integer ajax-input" maxLength="10" />
                            <label className="f-9 text-gray" >We will send an OTP to your entered mobile number for verification</label>
                        </div>
                        <div className="custom_focus">
                            <label className="ajaxlabel">Enter email id</label>
                            <input type="email" onChange={this.handleChange} id="email" className="ajax-input login_check_input integer ajax-input" />
                            <label className="f-9 text-gray" >We will send a link to your entered Email ID for verification</label>
                        </div>
                        <div className="clearfix"></div>

                        {/* <div className="text-center">
                                <input type="submit" className="ppm_btn_blue log_bt" value="SUBMIT" />
                            </div> */}
                        <div className="text-right">
                            <div className="align-items-center d-inline-flex justify-content-end">
                                <div onClick={this.StepValidation1} style={{ marginRight: '10px' }}>
                                    <span>NEXT</span>
                                </div>
                                <div>
                                    <FontAwesomeIcon style={{ fontSize: "30px" }} icon={faCaretRight} />
                                </div>
                            </div>
                        </div>
                        <Modal
                            aria-labelledby="contained-modal-title-vcenter"
                            centered show={this.state.show} onHide={this.handleClose}>
                            <Modal.Header>
                                <div style={{ color: '#012566' }} className="f-14 bold modal-title text-uppercase">Number Verification</div>
                            </Modal.Header>
                            <Modal.Body className="f-12">The privileges to use the Paytm withdrawal feature will be available only on this number.You will not be able to modify the number again.</Modal.Body>
                            <div style={{ backgroundColor: "#012566" }} className="flex text-white text-center">
                                <div onClick={this.handleClose} style={{ borderRight: "1px solid" }} className="col-6 verifsec">Change</div>
                                <div onClick={this.StepConfirm1} className="col-6 verifsec">Ok</div>
                            </div>
                        </Modal>
                    </form>
                </div>
            </div>
        );
    }
}



