import React, { Component } from 'react'
import SocialLogin from 'components/Login/SocialLogin';
import Steps from 'components/Login/Steps';
import StateDropdown from 'components/Login/StateDropdown';
import Referral from 'components/Login/Referral';
import { Form, Label, Group } from 'react-bootstrap';
import { webService } from 'components/common/webService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCoffee, faAngleRight, faCaretLeft } from '@fortawesome/free-solid-svg-icons';
export default class Step2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            stateData: [],
            referralShow: false
        }
    }

    getData() {
        let apiEndpoint = 'Oauth/getStates';
        let header = webService.getHeaders({})
        //console.log(postArray)
        webService.get(apiEndpoint, header).then((response) => {
            console.log(response);
            this.setState({
                stateData: response.data.states
                // stateDataabc: { comments: response.data.states }
            })
            //   console.log(this.state);
        })
    }

    showReferral = (e) => {
        this.setState({
            referralShow: true
            // stateDataabc: { comments: response.data.states }
        })
    }

    backOnSignUp = (e) => {
        this.props.history.push({
            pathname: '/signup',
            // mobile_no: mobile_no,
            // search: '?query=abc',
          //  state: { mobile_no: mobile_no, email: email }
        });
    }
    componentDidMount() {
        this.getData();
    }
    render() {
        console.log(this.props);
        const { stateData } = this.state;
        let stateList = '';
        if (stateData.length > 0) {
            // stateList = stateData.map(persons => <option>{persons.name}</option>)
            stateList = stateData.map(stateData => <StateDropdown stateData={stateData} />)
        }
        return (
            <div className="regmiddle">
                <SocialLogin />
                <div style={{ paddingTop: "0" }} className="login_box shadow bg-white relative">
                    <div className="flex justify-content-between align-items-center">
                        <div style={{ color: "#012566" }} className="bold">
                            SignUp
                            </div>
                        <div>
                            <div className="f-10 signin_right text-white">
                                Get Upto <span>&#8377; </span>100 Bonus Cash on sign-up
                                </div>
                        </div>
                    </div>
                    <Steps step={2} />
                    <form onSubmit="" className="login_check_form">
                        <div className="custom_focus form-group">
                            <div className="password_h_show" data-id="0">
                                <img className="height-20 vertically-center-bottom" src={require('assets/icons/hide_password.png')} />
                            </div>
                            <label className="ajaxlabel">Password</label>
                            <input type="password" onChange={this.handleChange} className="ajax-input password login_password" />
                            <label className="f-9 text-gray">Should be at least 8 characters</label>
                        </div>
                        <div controlId="exampleForm.SelectCustom">
                            <Form.Label>Select State</Form.Label>
                            <Form.Control as="select" custom>
                                <option>Select State</option>
                                {stateList}
                            </Form.Control>
                        </div>
                        <label className="f-9 text-gray">Should be at least 8 characters</label>
                    </form>
                    <div className="align-items-center flex justify-content-between">
                        <div className="align-items-center d-inline-flex justify-content-end">
                            <div>
                                <FontAwesomeIcon style={{ fontSize: "30px" }} icon={faCaretLeft} />
                            </div>
                            <div onClick={this.backOnSignUp} style={{ marginLeft: '10px' }}>
                                <span>BACK</span>
                            </div>
                        </div>
                        <div>
                            <button className="registerbtn text-white">Register</button>
                        </div>
                    </div>
                    <div onClick={this.showReferral} className=" bold d-inline-flex f-10 have_referral ppm-orange text-blue cursor">Have a referral code?</div>

                </div>
                {
                    (this.state.referralShow) ? <Referral referralShow={true} /> : null
                }
                {/* <Referral referralShow={this.state.referralShow} /> */}

            </div>


        )
    }
}
