const iState = {
    name: 'razi',
    age: 15,
    name1: 'razfewfwi',
    wishes: ['eat', 'code']
}

const reducer = (state = iState, action) => {
    console.log(action);
    switch (action.type) {
        case 'CHANGE_NAME':
            return {
                ...state,
                name: action.payload
            }
        case 'CHANGE_AGE':
            return {
                ...state,
                age: action.payload
            }
        default:
            return state
    }
}
export default reducer;