import React from 'react';
import LoginCheck from 'components/Login/LoginCheck';
import LoginMain from 'components/Login/LoginMain';
import SignUp from 'components/Login/SignUp';
import Step2 from 'components/Login/Step2';
import reduxCheck from 'reduxCheck';
import LoginBg from 'components/Login/LoginBg';
import { connect } from 'react-redux'
import { BrowserRouter, Route } from 'react-router-dom';
import { Redirect } from "react-router-dom";
import { Form, Control } from 'react-bootstrap'

function App(props) {
  console.log(props);
  return (
    <div className="App">
      <button onClick={() => { props.changeName("momin") }}>fe</button>
      <br />
    <button>Redux</button>
      <div>{props.myname}</div>
      <div className="main-container">
        <LoginBg />
        <BrowserRouter>
          <Route exact path="/" component={LoginCheck} />
          <Route path="/loginmain" component={LoginMain} />
          <Route path="/signup" component={SignUp} />
          <Route path="/step2" component={Step2} />
          <Route path="/redux" component={reduxCheck} />
        </BrowserRouter>
      </div>
    </div>
  );
}
const mapStateToProps = (state) => {
  return {
    myname: state.name,
    age: state.age,
    wishes: state.wishes,
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    changeName: (name) => {
      dispatch({
        type: 'CHANGE_NAME',
        payload: name
      })
    },
    changeAge: (age) => {
      dispatch({
        type: 'CHANGE_AGE',
        payload: age
      })
    }
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
